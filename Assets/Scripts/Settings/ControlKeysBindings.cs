using UnityEngine;

[CreateAssetMenu(fileName = "ControlKeysBindings", menuName = "Controls/Control Keys Bindings", order = 1)]
public static class ControlKeysBindings
{
    #region Fields

    [SerializeField] private static KeyCode run = KeyCode.LeftShift;

    [SerializeField] private static KeyCode walk = KeyCode.C;

    [SerializeField] private static KeyCode dash = KeyCode.LeftAlt;

    [SerializeField] private static KeyCode jump = KeyCode.Space;

    [SerializeField] private static KeyCode sit = KeyCode.LeftControl;

    [SerializeField] private static KeyCode grab = KeyCode.F;

    [SerializeField] private static KeyCode attack = KeyCode.Mouse0;

    #endregion Fields

    #region Properties

    public static KeyCode Run => run;

    public static KeyCode Walk => walk;

    public static KeyCode Dash => dash;

    public static KeyCode Jump => jump;

    public static KeyCode Sit => sit;

    public static KeyCode Grab => grab;

    public static KeyCode Attack => attack;

    #endregion Properties
}
