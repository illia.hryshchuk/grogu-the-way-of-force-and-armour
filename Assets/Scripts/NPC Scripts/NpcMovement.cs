using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(NpcCharacteristics))]
[RequireComponent(typeof(NpcInfo))]
[RequireComponent(typeof(NpcAttack))]
public class NpcMovement : MonoBehaviour
{
    #region Fields

    [SerializeField] private float patrolRadius = 20.0f;

    [SerializeField] private float walkTime = 8.0f; // While patroling - pnc walks this amount of time.
    [SerializeField] private float stayTime = 3.5f; // While patroling - pnc stays this amount of time.

    [SerializeField] private float maximalChasingTime = 15.0f; // After this time of unsuccess chasing - npc return back to its area.
    [SerializeField] private float maximalChasingDistance = 20.0f; // If npc is farther than this distance - it return back to its area.
    [SerializeField] private float startChasingDistance = 5.0f; // If player is closer to npc than this distance - it starts chasing.

    private Animator animator;
    private NpcCharacteristics npcCharacteristics;
    private CharacterController controller;
    private NpcInfo npcInfo;
    private Vector3 direction;

    private Transform target;
    private Vector3 startPositin;
    private float distanceToTarget = 0.0f;
    private float distanceToStart = 0.0f;

    private float walkTimeLeft = 0.0f;
    private float stayTimeLeft = 0.0f;
    private float chasingTime = 0.0f;

    private bool isStaying = true;
    private bool isWalking = false;
    private bool isChasing = false;
    private bool isReturning = false;
    private bool isAdditionalIdleLounched = false;
    private bool isMovingBlocked = false;

    #endregion Fields

    #region Standard Methods

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        controller = GetComponent<CharacterController>();
        npcCharacteristics = GetComponent<NpcCharacteristics>();
        npcInfo = GetComponent<NpcInfo>();
        target = GameObject.FindWithTag("Player").transform;
        startPositin = transform.position;

        distanceToTarget = Vector3.Distance(transform.position, target.transform.position);
        distanceToStart = Vector3.Distance(transform.position, startPositin);
    }

    void Update()
    {
        if(isMovingBlocked)
        {
            return;
        }

        if(npcCharacteristics.IsDead())
        {
            animator.SetTrigger("Die");
            isMovingBlocked = true;
            return;
        }

        distanceToTarget = Vector3.Distance(transform.position, target.transform.position);
        distanceToStart = Vector3.Distance(transform.position, startPositin);

        if (distanceToTarget < startChasingDistance && !isReturning && npcInfo.IsAggressive)
        {
            isChasing = true;
        }

        if (isChasing)
        { 
            Chase();
        }
        else if (isReturning)
        {
            ReturnToPatrolArea();
        }
        else
        {
            Patrol();
        }

        UpdateGravity();
        controller.Move(direction);
    }

    #endregion Standard Methods

    #region Private Methods

    private void Chase()
    {
        transform.LookAt(target.transform);
        direction = transform.TransformDirection(0.0f, 0.0f, 1.0f);
        direction *= npcCharacteristics.RunSpeed * Time.deltaTime;

        chasingTime += Time.deltaTime;
        if(distanceToTarget < npcCharacteristics.AttackDistance)
        {
            chasingTime = 0.0f;
        }
        if(distanceToTarget <= GameConstants.MinAttackDistance)
        {
            direction = transform.TransformDirection(0.0f, 0.0f, 0.0f);
            animator.SetBool("IsWaitingAttack", true);
        }
        else
        {
            animator.SetBool("IsWaitingAttack", false);
        }
        if(chasingTime > maximalChasingTime || distanceToTarget > maximalChasingDistance)
        {
            isChasing = false;
            isReturning = true;
        }

        animator.SetBool("IsRunning", true);
        animator.SetBool("IsWalking", false);
    }

    private void ReturnToPatrolArea()
    {
        transform.LookAt(startPositin);
        direction = transform.TransformDirection(0.0f, 0.0f, 1.0f);
        direction *= npcCharacteristics.RunSpeed * Time.deltaTime;

        if(distanceToStart < patrolRadius * 0.7f)
        {
            isReturning = false;
        }

        animator.SetBool("IsRunning", true);
        animator.SetBool("IsWalking", false);
    }

    private void Patrol()
    {
        if (isWalking)
        {
            // Check if there are no borders.
            float currentHeight = Terrain.activeTerrain.SampleHeight(transform.position);
            Vector3 forwardPoint = transform.position + (transform.forward * 7.0f);
            float forwardHeight = Terrain.activeTerrain.SampleHeight(forwardPoint);
            bool isNearObstacle = Mathf.Abs(currentHeight - forwardHeight) > 8.0f;

            // If npc is out of patrol area - turn to area direction.
            if (distanceToStart > patrolRadius || isNearObstacle)
            {
                transform.LookAt(startPositin);

                direction = transform.TransformDirection(0.0f, 0.0f, 1.0f);
                direction *= npcCharacteristics.WalkSpeed * Time.deltaTime;
            }
            else // If npc in patrol area - go straight till timer ends.
            {
                direction = transform.TransformDirection(0.0f, 0.0f, 1.0f);
                direction *= npcCharacteristics.WalkSpeed * Time.deltaTime;
            }

            walkTimeLeft -= Time.deltaTime;
            if (walkTimeLeft <= 0)
            {
                isStaying = true;
                isWalking = false;
                isAdditionalIdleLounched = false;
                walkTimeLeft = Random.Range(walkTime * 0.8f, walkTime * 1.2f);
                stayTimeLeft = Random.Range(stayTime * 0.8f, stayTime * 1.2f); ;
            }
        }
        else if (isStaying) // If npc is staying - stay till timer end, and than turn randomely.
        {
            direction = transform.TransformDirection(0.0f, 0.0f, 0.0f);
            stayTimeLeft -= Time.deltaTime;

            if (!isAdditionalIdleLounched && stayTimeLeft < stayTime/2.0f)
            {
                if (UtilMethods.CountProbability(1.0f))
                {
                    isAdditionalIdleLounched = true;
                    animator.SetTrigger("AdditionalIdle");
                }
            }

            if (stayTimeLeft <= 0)
            {
                isStaying = false;
                isWalking = true;
                stayTimeLeft = Random.Range(stayTime * 0.8f, stayTime * 1.2f);
                walkTimeLeft = Random.Range(walkTime * 0.8f, walkTime * 1.2f);

                float offset = 75.0f;
                transform.Rotate(new Vector3(0.0f, 1.0f, 0.0f), Random.Range(-offset, offset));
            }
        }
        else if ((isWalking && isStaying) || (!isWalking && !isStaying))
        {
            isStaying = false;
            isWalking = true;
            stayTimeLeft = Random.Range(stayTime * 0.8f, stayTime * 1.2f);
            walkTimeLeft = Random.Range(walkTime * 0.8f, walkTime * 1.2f);
        }

        animator.SetBool("IsRunning", false);
        animator.SetBool("IsWalking", isWalking);
    }

    private void UpdateGravity()
    {
        direction.y -= GameConstants.Gravity * Time.deltaTime;
        if (controller.isGrounded && direction.y < 0.0f)
        {
            direction.y = 0.0f;
        }
    }

    #endregion Private Methods

}
