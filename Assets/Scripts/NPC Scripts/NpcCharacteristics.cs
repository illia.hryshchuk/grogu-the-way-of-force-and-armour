using UnityEngine;

public class NpcCharacteristics : MonoBehaviour
{
    #region Fields

    [SerializeField] [Range(GameConstants.MinHP, GameConstants.MaxHP)] private int totalHP = 100;
    [Range(GameConstants.MinHP, GameConstants.MaxHP)] private int currentHP = 100;
    [SerializeField] [Range(GameConstants.MinFP, GameConstants.MaxFP)] private int totalFP = 100;
    [Range(GameConstants.MinFP, GameConstants.MaxFP)] private int currentFP = 100;

    [SerializeField] [Range(GameConstants.MinMoveSpeed, GameConstants.MaxMoveSpeed)] private float runSpeed = 5.0f;
    [SerializeField] [Range(GameConstants.MinMoveSpeed, GameConstants.MaxMoveSpeed)] private float walkSpeed = 0.5f;

    [SerializeField] [Range(GameConstants.MinAttackPower, GameConstants.MaxAttackPower)] private int attackPower = 5;
    [SerializeField] [Range(GameConstants.MinAttackSpeed, GameConstants.MaxAttackSpeed)] private float attackSpeed = 4.0f;
    [SerializeField] [Range(GameConstants.MinAttackDistance, GameConstants.MaxAttackDistance)] private float attackDistance = 2.0f;
    [SerializeField] [Range(GameConstants.MinCritRate, GameConstants.MaxCritRate)] private float critRate = 0.1f;

    #endregion Fields

    #region Properties

    public int CurrentHP => currentHP;

    public int CurrentFP => currentFP;

    public int TotalHP => totalHP;

    public int TotalFP => totalFP;

    public float RunSpeed => runSpeed;

    public float WalkSpeed => walkSpeed;

    public int AttackPower => attackPower;

    public float AttackSpeed => attackSpeed;

    public float AttackDistance => attackDistance;

    public float CritRate => critRate;

    #endregion Properties

    #region Standard Methods

    void Start()
    {
        currentHP = TotalHP;
        currentFP = totalFP;
    }

    #endregion Standard Methods

    #region Public Methods

    public void TakeDamage(int damage)
    {
        if(currentHP < damage)
        {
            currentHP = 0;
        }
        else
        {
            currentHP -= damage;
        }
    }

    public bool IsDead()
    {
        return currentHP <= 0;
    }

    #endregion Public Methods
}
