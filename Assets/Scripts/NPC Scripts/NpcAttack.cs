using UnityEngine;

[RequireComponent(typeof(NpcCharacteristics))]
[RequireComponent(typeof(NpcInfo))]
public class NpcAttack : MonoBehaviour
{
    #region Fields

    private Animator animator;
    private NpcCharacteristics npcCharacteristics;
    private PlayerCharacteristics playerCharacteristics;
    private Transform target;
    private NpcInfo npcInfo;

    private float distanceToTarget = 0.0f;
    private float attackTimeLeft = 0.0f;

    #endregion Fields

    #region Standard Methods

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        npcCharacteristics = GetComponent<NpcCharacteristics>();
        playerCharacteristics = GameObject.FindWithTag("Player").GetComponent<PlayerCharacteristics>();
        target = GameObject.FindWithTag("Player").transform;
        npcInfo = GetComponent<NpcInfo>();

        distanceToTarget = Vector3.Distance(transform.position, target.transform.position);
    }

    void Update()
    {
        if (npcCharacteristics.IsDead() || !npcInfo.IsAggressive)
        {
            return;
        }

        distanceToTarget = Vector3.Distance(transform.position, target.transform.position);

        TryAttack();
    }

    #endregion Standard Methods

    #region Private Methods

    private void TryAttack()
    {
        if (distanceToTarget < npcCharacteristics.AttackDistance)
        {
            if (attackTimeLeft < 0.0f)
            {
                int damage = UtilMethods.CalculateDamage(npcCharacteristics);

                if (UtilMethods.CountProbability(30.0f))
                {
                    animator.SetTrigger("PowerAttack");
                    damage = (int)(damage * 1.2f);
                }
                else
                {
                    animator.SetTrigger("Attack");
                }
                
                transform.LookAt(target.transform);
                playerCharacteristics.TakeDamage(damage);
                attackTimeLeft = npcCharacteristics.AttackSpeed;

                Debug.Log("Damaged: " + damage);
            }
        }

        attackTimeLeft -= Time.deltaTime;
        if (attackTimeLeft < -1.0f)
        {
            attackTimeLeft = -1.0f;
        }
    }

    #endregion Private Methods

}
