using UnityEngine;

public class NpcInfo : MonoBehaviour
{
    #region Fields

    [SerializeField] private string npcName = "Unknown name";

    [SerializeField] private string npcDescription = "Unknown description";

    [SerializeField] private string npcType = "Unknown type";

    [SerializeField] private bool isAggressive = false;

    #endregion Fields

    #region Properties

    public string NpcName => npcName;

    public string NpcDescription => npcDescription;

    public string NpcType => npcType;

    public bool IsAggressive => isAggressive;

    #endregion Properties
}
