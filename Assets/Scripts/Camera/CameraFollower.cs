using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    #region Fields

    public Vector3 cameraOffset;

    public bool cursorLocked = false;
    public float xSensitivity = 7;
    public float ySensitivity = 7;
    public float zoomSensitivity = 1;

    [Range(0.5f, 15f)] public float zoomMinDistance = 1;
    [Range(0.5f, 15f)] public float zoomMaxDistance = 5;
    [Range(-180f, 180f)] public float yawMinLimit = -88;
    [Range(-180f, 180f)] public float yawMaxLimit = 88;

    private Transform target;
    private float pitch, yaw, zoomValue;
    private const float startCameraAngle = 23;
    private const float rotationSmoothTime = .12f;
    private Vector3 rotationSmoothVelocity;
    private Vector3 currentRotation;

    #endregion Fields

    #region Standard Methods

    void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
        transform.position = target.transform.position + cameraOffset;
        transform.rotation = Quaternion.Euler(startCameraAngle, 0, 0);
        pitch = startCameraAngle;

        if (cursorLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    void Update()
    {
        zoomValue = Input.GetAxis("Mouse ScrollWheel");

        if (Input.GetMouseButton(1))
        {
            pitch -= Input.GetAxis("Mouse Y") * ySensitivity;
            yaw += Input.GetAxis("Mouse X") * xSensitivity;
            pitch = Mathf.Clamp(pitch, yawMinLimit, yawMaxLimit);

            currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(pitch, yaw), ref rotationSmoothVelocity, rotationSmoothTime);
            transform.eulerAngles = currentRotation;
        }

        transform.position = target.transform.position + transform.localRotation * cameraOffset;
        UpdateZoom();
    }

    #endregion Standard Methods

    #region Private Methods

    private void UpdateZoom()
    {
        if (zoomValue > 0)
        {
            cameraOffset.z += zoomSensitivity;
        }
        if (zoomValue < 0)
        {
            cameraOffset.z -= zoomSensitivity;
        }
        cameraOffset.z = Mathf.Clamp(cameraOffset.z, -zoomMaxDistance, -zoomMinDistance);
    }

    #endregion Private Methods
}
