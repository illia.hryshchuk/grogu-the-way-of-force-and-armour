using UnityEngine;


public class ItemInfo : MonoBehaviour
{
    #region Fields

    [SerializeField] private string itemName = "Unknown item";

    [SerializeField] private string itemDescription = "Unknown description";

    [SerializeField] private string itemType = "Unknown type";

    #endregion Fields

    #region Properties

    public string ItemName => itemName;

    public string ItemDescription => itemDescription;

    public string ItemType => itemType;

    #endregion Properties
}
