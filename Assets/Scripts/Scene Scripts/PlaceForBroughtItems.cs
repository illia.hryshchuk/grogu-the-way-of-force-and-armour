using UnityEngine;
using UnityEngine.UI;

public class PlaceForBroughtItems : MonoBehaviour
{
    #region Fields

    public int currentItemsNumber = 0;
    public int finalItemsNumber = 5;

    public Text itemsBringingText;

    #endregion Fields

    #region Standard Methods

    private void Start()
    {
        itemsBringingText.text = $"Items to bring left: {currentItemsNumber}/{finalItemsNumber}";
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody)
        {
            var itemInfo = other.attachedRigidbody.GetComponent<QuestItem>();
            if (itemInfo != null)
            {
                if (itemInfo.IsQuestItem)
                {
                    currentItemsNumber += 1;
                    UpdateUI();
                }
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.attachedRigidbody)
        {
            var itemInfo = other.attachedRigidbody.GetComponent<QuestItem>();
            if (itemInfo != null)
            {
                if (itemInfo.IsQuestItem)
                {
                    currentItemsNumber -= 1;
                    UpdateUI();
                }
            }
        }
    }

    #endregion Standard Methods

    #region Private Methods

    private void UpdateUI()
    {
        itemsBringingText.text = $"Items to bring left: {currentItemsNumber}/{finalItemsNumber}";
    }

    #endregion Private Methods

}