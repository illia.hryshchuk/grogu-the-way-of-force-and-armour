using UnityEngine;

public class QuestItem : ItemInfo
{
    #region Fields

    [SerializeField] private bool isQuestItem = true;

    #endregion Fields

    #region Properties

    public bool IsQuestItem => isQuestItem;

    #endregion Properties

}
