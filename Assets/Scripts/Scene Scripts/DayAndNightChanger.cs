using UnityEngine;

public class DayAndNightChanger : MonoBehaviour
{
    #region Fields

    [SerializeField] private int dayDuration = 1800;
    [SerializeField] private float directionSmoothing = 0.5f; 

    private float timeAfterChange = 0.0f;
    private float changeTime;
    private Vector3 axis;

    #endregion Fields

    #region Standard Methods

    void Start()
    {
        axis = new Vector3(1.0f, 0.0f, 0.0f);
        changeTime = (dayDuration * directionSmoothing) / 360.0f;
    }

    void Update()
    {
        timeAfterChange += Time.deltaTime;

        if(timeAfterChange > changeTime)
        {
            timeAfterChange = 0.0f;
            transform.Rotate(axis, directionSmoothing);
        }
    }

    #endregion Standard Methods
}
