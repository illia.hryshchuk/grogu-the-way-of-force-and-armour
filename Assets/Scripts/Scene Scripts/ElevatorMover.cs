using UnityEngine;
public class ElevatorMover : MonoBehaviour
{
    #region Fields

    [SerializeField] private Vector3 startPosition;
    [SerializeField] private Vector3 endPosition;
    [SerializeField] private float speed = 5.0f;
    [SerializeField] private float pause = 5.0f;

    private float distanceToDestination = 0.0f;
    private float stayingTime = 0.0f;
    private bool isWorking = true;
    private bool isGoingToEnd = true;
    private bool isStaying = false;

    #endregion Fields

    #region Standard Methods

    void Update()
    {
        if(isWorking)
        {
            if(isStaying)
            {
                stayingTime += Time.deltaTime;

                if(stayingTime > pause)
                {
                    isStaying = false;
                    stayingTime = 0.0f;
                }
                return;
            }

            if (isGoingToEnd)
            {
                distanceToDestination = Vector3.Distance(transform.position, endPosition);
                if (distanceToDestination < GameConstants.Tolerance)
                {
                    isStaying = true;
                    isGoingToEnd = false;
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, endPosition, speed * Time.deltaTime);
                }
            }
            else
            {
                distanceToDestination = Vector3.Distance(transform.position, startPosition);
                if (distanceToDestination < GameConstants.Tolerance)
                {
                    isStaying = true;
                    isGoingToEnd = true;
                }
                else
                {
                    transform.position = Vector3.MoveTowards(transform.position, startPosition, speed*Time.deltaTime);
                }
            }
        }
    }

    #endregion Standard Methods
}
