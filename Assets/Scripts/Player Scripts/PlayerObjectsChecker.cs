using UnityEngine;

public class PlayerObjectsChecker : MonoBehaviour
{
    #region Fields

    private GameObject textInfoObject;
    private Transform textInfoPosition;
    private TextMesh textInfo;

    private Transform playerLook;
    private LayerMask layerMask;

    public Vector3 offset;
    private Vector3 halfRectSize;

    #endregion Fields

    #region Standard Methods

    void Start()
    {
        textInfoObject = GameObject.FindWithTag("ItemInfoText");
        textInfoPosition = textInfoObject.transform;
        textInfo = textInfoObject.GetComponent<TextMesh>();

        playerLook = GameObject.FindWithTag("MainCamera").transform;
        layerMask = LayerMask.GetMask(LayerNames.npc, LayerNames.pickableItem, LayerNames.notInterractiveObject, LayerNames.playableArea);
        offset = new Vector3(0.0f, 1.5f, 0.0f);
        halfRectSize = new Vector3(GameConstants.MaxHintColliderDistance, GameConstants.MaxHintColliderDistance, GameConstants.MaxHintColliderDistance);
    }

    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(playerLook.transform.position, playerLook.transform.forward, out hit, GameConstants.MaxHintRayDistance, layerMask))
        {
            // Show text and place it over the item.
            textInfoObject.SetActive(true);
            offset.y = hit.collider.bounds.size.y;
            textInfoPosition.position = hit.collider.transform.position + offset;
            textInfoPosition.LookAt(playerLook.transform.position);

            // Add text.
            textInfo.text = GetItemInfoText(hit.collider);
        }
        else
        {
            var inShowTrigger = Physics.OverlapBox(transform.position, halfRectSize, Quaternion.identity, layerMask);

            if (inShowTrigger.Length > 0)
            {
                foreach(var obj in inShowTrigger)
                {
                    string itemText = GetItemInfoText(obj);
                    if(itemText != "")
                    {
                        // Set text as active.
                        textInfoObject.SetActive(true);
                        offset.y = obj.bounds.size.y;
                        textInfoPosition.position = obj.transform.position + offset;
                        textInfoPosition.LookAt(playerLook.transform.position);

                        // Add text.
                        textInfo.text = GetItemInfoText(obj);
                        break;
                    }
                }
            }
            else
            {
                // Deactivate text.
                textInfoObject.SetActive(false);
            }
        }
    }

    #endregion Standard Methods

    #region Private Methods

    private string GetItemInfoText(Collider collider)
    {
        // Read text info.
        string showableText = $"";

        // If it is item.
        ItemInfo itemInfo = collider.gameObject.GetComponent<ItemInfo>();
        if (itemInfo)
        {
            showableText += itemInfo.ItemName;
        }
        else
        {
            // If it is npc.
            NpcInfo npcInfo = collider.gameObject.GetComponent<NpcInfo>();
            if (npcInfo)
            {
                showableText += npcInfo.NpcName;
                NpcCharacteristics chars = collider.gameObject.GetComponent<NpcCharacteristics>();
                if (chars)
                {
                    showableText += $"\n" + $"HP: {chars.CurrentHP}/{chars.TotalHP}";
                }
            }
        }

        return showableText;
    }

    #endregion Private Methods

}
