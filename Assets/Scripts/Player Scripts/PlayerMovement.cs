using UnityEngine;

[RequireComponent(typeof(PlayerCharacteristics))]
[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
    #region Fields

    private Transform thirdCamera;
    private CharacterController controller;
    private PlayerCharacteristics characteristics;
    private Vector3 direction;

    private float vertical, horizontal;
    private bool isDashing;
    private float dashTimeLeft = 0.0f;

    private int jumpCounter = 0;
    private float timeAfterJump = 0.0f;
    private float verticalVelocity = 0;

    private const float distanceToGround = 0.4f;
    private float characterHeight;
    private bool isOnGround = false;
    private const float tolerance = 0.001f;

    #endregion Fields

    #region Properties

    private bool IsNotMoving => horizontal == 0.0f && vertical == 0.0f;

    private float JumpValue => Mathf.Sqrt(characteristics.JumpHeight * 2.0f * GameConstants.Gravity);

    #endregion Properties

    #region Standard Methods

    void Start()
    {
        controller = GetComponent<CharacterController>();
        characteristics = GetComponent<PlayerCharacteristics>();
        thirdCamera = GameObject.FindWithTag("MainCamera").transform;
        characterHeight = controller.height;
    }

    void FixedUpdate()
    {
        // Get move direction.
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        // Do all transformations.
        UpdateMovement();
        UpdateJump();
        UpdateDash();
        UpdateSit();
        UpdateGravity();

        // Apply transformation.
        controller.Move(direction);
    }

    #endregion Standard Methods

    #region Private Methods

    private void UpdateGravity()
    {
        verticalVelocity -= GameConstants.Gravity * Time.deltaTime;
        direction.y = verticalVelocity * Time.deltaTime;

        isOnGround = Physics.Raycast(transform.position, -Vector3.up, distanceToGround);
        if (controller.isGrounded && direction.y < 0.0f)
        {
            direction.y = 0.0f;
            verticalVelocity = 0.0f;
        }
    }

    private void UpdateMovement(bool goForward = false)
    {
        // Move player.
        direction = goForward ? transform.TransformDirection(0.0f, 0.0f, 1.0f) : transform.TransformDirection(horizontal, 0.0f, vertical);
        direction *= GetCurrentSpeed() * Time.deltaTime;

        // Rotate player with mouse.
        if (IsNotMoving)
        {
            return;
        }

        Vector3 target = thirdCamera.forward;
        target.y = 0.0f;

        Quaternion look = Quaternion.LookRotation(target);
        float speed = characteristics.AngularSpeed * Time.deltaTime;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, look, speed);
    }

    private void UpdateJump()
    {
        bool isJumpAllowed = timeAfterJump > characteristics.JumpRepeatTime;

        if (Input.GetKeyDown(ControlKeysBindings.Jump) && isJumpAllowed)
        {
            if (isOnGround)
            {
                jumpCounter = 0;
                verticalVelocity = JumpValue;
                ++jumpCounter;
            }
            else if (jumpCounter < characteristics.JumpNumberAllowed)
            {
                verticalVelocity = JumpValue;
                ++jumpCounter;
            }
            timeAfterJump = 0.0f;
        }
        if (timeAfterJump < characteristics.JumpRepeatTime + tolerance)
        {
            timeAfterJump += Time.deltaTime;
        }
    }

    private void UpdateDash()
    {
        bool readyToDash = dashTimeLeft < -characteristics.DashRepeatTime + tolerance;
        if (Input.GetKeyDown(ControlKeysBindings.Dash) && readyToDash)
        {
            isDashing = true;
            dashTimeLeft = characteristics.DashTime;
        }

        if (dashTimeLeft > -characteristics.DashRepeatTime)
        {
            dashTimeLeft -= Time.deltaTime;
        }
        if (dashTimeLeft <= 0.0f)
        {
            isDashing = false;
        }
        if (isDashing)
        {
            UpdateMovement(IsNotMoving);
        }
    }

    private void UpdateSit()
    {
        if (Input.GetKey(ControlKeysBindings.Sit) && !isDashing)
        {
            controller.height = characterHeight * 0.5f;
        }
        else
        {
            controller.height = characterHeight;
        }
    }

    private float GetCurrentSpeed()
    {
        float speed = characteristics.MoveSpeed;
        if (isDashing)
        {
            speed = characteristics.DashSpeed;
        }
        else if (Input.GetKey(ControlKeysBindings.Walk) || Input.GetKey(ControlKeysBindings.Sit))
        {
            speed = characteristics.WalkSpeed;
        }
        else if (Input.GetKey(ControlKeysBindings.Run))
        {
            speed = characteristics.RunSpeed;
        }

        return speed;
    }

    #endregion Private Methods
}
