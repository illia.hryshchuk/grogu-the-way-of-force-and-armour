using UnityEngine;

[RequireComponent(typeof(PlayerCharacteristics))]
public class PlayerAttack : MonoBehaviour
{
    private PlayerCharacteristics characteristics;
    private LayerMask layerMask;
    private Vector3 halfRectSize;

    void Start()
    {
        characteristics = GetComponent<PlayerCharacteristics>();
        layerMask = LayerMask.GetMask(LayerNames.npc);
        halfRectSize = new Vector3(characteristics.AttackDistance, characteristics.AttackDistance, characteristics.AttackDistance);
}

    void Update()
    {
        if (Input.GetKeyDown(ControlKeysBindings.Attack))
        {
            var inAttackTrigger = Physics.OverlapBox(transform.position, halfRectSize, Quaternion.identity, layerMask);

            foreach (var obj in inAttackTrigger)
            {
                var npcChars = obj.gameObject.GetComponent<NpcCharacteristics>();
                if(npcChars)
                {
                    int damage = CalculateDamage(characteristics);
                    npcChars.TakeDamage(damage);
                    break;
                }
            }
        }
    }

    private int CalculateDamage(NpcCharacteristics characteristics)
    {
        // Get randomely +- 35%
        float randModifier = 1 + Random.Range(-GameConstants.DamageRandomPercentage, GameConstants.DamageRandomPercentage);

        // Count damage.
        float damage = characteristics.AttackPower * randModifier;

        // Calculate chance of critical damage.
        int per = Random.Range(0, 100);
        if (per < characteristics.CritRate * 100)
        {
            float critModifier = Random.Range(GameConstants.MinCritMultiplier, GameConstants.MaxCritMultiplier);
            damage *= critModifier;
        }

        // Return damage.
        return (int)damage;
    }
}
