using UnityEngine;

public class PlayerCharacteristics : NpcCharacteristics
{
    #region Fields

    [SerializeField] [Range(GameConstants.MinMoveSpeed, GameConstants.MaxMoveSpeed)] private float moveSpeed = 3.0f;

    [SerializeField] private float dashSpeed = 30.0f;

    [SerializeField] private float dashTime = 0.16f;

    [SerializeField] private float dashRepeatTime = 0.7f;

    [SerializeField] private float jumpHeight = 1.0f;

    [SerializeField] private int jumpNumberAllowed = 2;

    [SerializeField] private float jumpRepeatTime = 0.15f;

    [SerializeField] private float angularSpeed = 1500.0f;

    #endregion Fields

    #region Properties

    public float MoveSpeed => moveSpeed;

    public float DashSpeed => dashSpeed;

    public float DashTime => dashTime;

    public float DashRepeatTime => dashRepeatTime;

    public float JumpHeight => jumpHeight;

    public int JumpNumberAllowed => jumpNumberAllowed;

    public float JumpRepeatTime => jumpRepeatTime;

    public float AngularSpeed => angularSpeed;

    #endregion Properties
}
