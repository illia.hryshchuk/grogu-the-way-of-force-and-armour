using UnityEngine;

public class PlayerGrab : MonoBehaviour
{
    #region Fields

    public float grabRadius = 1.0f;
    public float grabRepeatTime = 0.5f;
    public Vector3 offset = new Vector3(0.8f, 0.6f, 0.6f);

    private Rigidbody grabbedObject;
    private int grabLayerMask;

    private const float tolerance = 0.001f;
    private float grabCooldown;

    private Vector3 halfRectSize;

    #endregion Fields

    #region Standard Methods

    private void Start()
    {
        grabLayerMask = LayerMask.GetMask(LayerNames.pickableItem);
        halfRectSize = new Vector3(grabRadius, grabRadius, grabRadius);
    }

    void FixedUpdate()
    {
        grabCooldown -= Time.deltaTime;
        if(grabCooldown > 0.0f)
        {
            return;
        }

        if (Input.GetKeyDown(ControlKeysBindings.Grab))
        {
            if (!IsSomethingGrabbed())
            {
                TryGrabClosestItem();
            }
            else
            {
                Release();
            }
        }

        if (grabCooldown < 0.0f)
        {
            grabCooldown = -tolerance;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, grabRadius);
    }

    #endregion Standard Methods

    #region Private Methods

    private void TryGrabClosestItem()
    {
        var inGrabTrigger = Physics.OverlapBox(transform.position, halfRectSize, Quaternion.identity, grabLayerMask);//Physics.OverlapSphere(transform.position, grabRadius, grabLayerMask);

        if (inGrabTrigger.Length > 0)
        {
            foreach (var rig in inGrabTrigger)
            {
                Rigidbody target = rig.attachedRigidbody;
                if (target)
                {
                    if (!target.isKinematic && target.gameObject.layer == LayerMask.NameToLayer(LayerNames.pickableItem))
                    {
                        grabbedObject = target;
                        grabbedObject.isKinematic = true;
                        grabbedObject.transform.SetParent(transform, true);
                        grabbedObject.transform.SetPositionAndRotation(transform.position + offset, transform.rotation);

                        grabCooldown = grabRepeatTime;
                    }
                    break;
                }
            }
        }
    }

    private void Release()
    {
        if (grabbedObject != null)
        {
            grabbedObject.transform.SetParent(null, true);
            grabbedObject.isKinematic = false;
            grabbedObject = null;

            grabCooldown = grabRepeatTime;
        }
    }

    private bool IsSomethingGrabbed()
    {
        return grabbedObject != null;
    }

    #endregion Public Methods
}