using UnityEngine;

public static class LayerNames
{
    #region Fields

    public const string pickableItem = "Pickable Item";

    public const string npc = "NPC";

    public const string playableArea = "Playable Area";

    public const string notInterractiveObject = "Not Interractive Object";

    #endregion Fields
}
