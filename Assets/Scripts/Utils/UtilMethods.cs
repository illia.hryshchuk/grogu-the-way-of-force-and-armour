using UnityEngine;

static public class UtilMethods
{
    #region Public Methods

    public static int CalculateDamage(NpcCharacteristics characteristics)
    {
        // Get randomely +- 35%
        float randModifier = 1 + Random.Range(-GameConstants.DamageRandomPercentage, GameConstants.DamageRandomPercentage);

        // Count damage.
        float damage = characteristics.AttackPower * randModifier;

        // Calculate chance of critical damage.
        int per = Random.Range(0, 100);
        if (per < characteristics.CritRate * 100)
        {
            float critModifier = Random.Range(GameConstants.MinCritMultiplier, GameConstants.MaxCritMultiplier);
            damage *= critModifier;
        }

        // Return damage.
        return (int)damage;
    }

    public static bool CountProbability(float percents)
    {
        float rand = Random.Range(0.0f, 100.0f);
        return rand < percents;
    }

    #endregion Public Methods
}
