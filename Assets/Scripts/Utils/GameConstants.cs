using UnityEngine;

public static class GameConstants
{
    #region Fields

    /// <summary>
    /// Gravity force.
    /// </summary>
    public const float Gravity = 25.0f;

    /// <summary>
    /// Number of attacks per minute.
    /// </summary>
    public const float MaxMoveSpeed = 8.0f;

    /// <summary>
    /// Number of attacks per minute.
    /// </summary>
    public const float MinMoveSpeed = 0.01f;

    /// <summary>
    /// Attack every MaxAttackSpeed seconds.
    /// </summary>
    public const float MaxAttackSpeed = 0.5f;

    /// <summary>
    /// Attack every MinAttackSpeed seconds.
    /// </summary>
    public const float MinAttackSpeed = 60.0f;

    /// <summary>
    /// Maximal crit probability.
    /// </summary>
    public const float MaxCritRate = 0.8f;

    /// <summary>
    /// Minimal crit probability.
    /// </summary>
    public const float MinCritRate = 0.05f;

    /// <summary>
    /// Maximal critical multiplier.
    /// </summary>
    public const float MaxCritMultiplier = 3.0f;

    /// <summary>
    /// Minimal critical multiplier.
    /// </summary>
    public const float MinCritMultiplier = 1.2f;

    /// <summary>
    /// Maximal attack power.
    /// </summary>
    public const int MaxAttackPower = 10000;

    /// <summary>
    /// Minimal attack power.
    /// </summary>
    public const int MinAttackPower = 1;

    /// <summary>
    /// Minimal attack distance.
    /// </summary>
    public const float MinAttackDistance = 1.7f;

    /// <summary>
    /// MAximal attack distance.
    /// </summary>
    public const float MaxAttackDistance = 15.0f;

    /// <summary>
    /// Damage random.
    /// </summary>
    public const float DamageRandomPercentage = 0.35f;

    /// <summary>
    /// Maximal HP.
    /// </summary>
    public const float MaxHP = 10000.0f;

    /// <summary>
    /// Minimal HP.
    /// </summary>
    public const float MinHP = 10.0f;

    /// <summary>
    /// Maximal FP.
    /// </summary>
    public const float MaxFP = 10000.0f;

    /// <summary>
    /// Minimal FP.
    /// </summary>
    public const float MinFP = 10.0f;

    /// <summary>
    /// Max hint distance vie ray.
    /// </summary>
    public const float MaxHintRayDistance = 30.0f;

    /// <summary>
    /// Max hint distance via collider.
    /// </summary>
    public const float MaxHintColliderDistance = 4.0f;

    /// <summary>
    /// Project tolerance.
    /// </summary>
    public const float Tolerance = 0.0001f;

    #endregion Fields
}
