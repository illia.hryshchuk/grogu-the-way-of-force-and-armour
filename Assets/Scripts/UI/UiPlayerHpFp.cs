using UnityEngine;
using UnityEngine.UI;

public class UiPlayerHpFp : MonoBehaviour
{
    private PlayerCharacteristics playerCharacteristics;
    private Text HPText;
    private Text FPText;

    void Start()
    {
        HPText = GameObject.FindWithTag("HPText").GetComponent<Text>();
        FPText = GameObject.FindWithTag("FPText").GetComponent<Text>();

        playerCharacteristics = GameObject.FindWithTag("Player").GetComponent<PlayerCharacteristics>();
    }

    void Update()
    {
        HPText.text = $"HP: {playerCharacteristics.CurrentHP}/{playerCharacteristics.TotalHP}";
        FPText.text = $"FP: {playerCharacteristics.CurrentFP}/{playerCharacteristics.TotalFP}";
    }
}
